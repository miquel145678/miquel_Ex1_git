int incomingByte = 0; // for incoming serial data
int inPin = 3;
int val = 0;
void setup() {
  pinMode(inPin, INPUT);
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  while(!Serial)
  {;}
  Serial.println("setup complete");
}

void loop() {
  // put your main code here, to run repeatedly:
  val = digitalRead(inPin);
  Serial.println(val,DEC);

}
