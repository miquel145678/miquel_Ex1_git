#include <Chrono.h>
#define DEBUGG
int QUITAR DESPUES DE GIT
//estados del protocolo
enum SendingStatus {
  disponible = 0,
  startSignal = 1,
  sendingPDU = 2,
  stopSignal = 3,    
};

//rols
enum Rol {
  master,
  slave
};

//OPCodis valids
enum OPCode {
  llegir_entrada = 1,
  llegir_sortida = 2,
  llegir_analogica = 3,
  llegir_registre = 4,
  escriure_sortida = 5,
  escriure_sortida_analogica = 6,
  escriure_registre = 7,
  error = 8 //només s'envia de l'esclau al master si hi ha hagut algu problema
};

//LLista de codis d'error possibles a retornar
enum codis_error {
  error_crc_missatge,
  operacio_no_reconeguda,
  operacio_no_definida,
  adresa_invalida,
  temps_espera_superat,
  error_process,
  error_desconegut = -1,
  no_error = 0  
};
//Definiciones de la trama llobrebus
int LONG_MAX_TRAMA = 11; // una trama llobrebus tendra màximo 11 bytes
//Definició de taula de caracters especials:
const char STARTCHAR = 0x3A;
const char STOPCHAR[2] = {0x0D, 0x0A};
//altres constants utils
const float SIGNALT_TIME_START = 3.5; //3.5 TB
const float SIGNALT_TIME_END = 3.5; //3.5 TB
const unsigned long TIME_OUT = 1000; //temps d'espera de resposta 1 segon
const int MAX_DATA_LENGTH = 4; //màxim número de bytes per a les dades

//structura per agrupar tots els camps duna PDU i poder separar missatges enviats de missatges rebuts

struct LLobrebus_PDU {
  int adress;
  OPCode operacio;
  byte data[MAX_DATA_LENGTH];
  byte crc[2];
};

LLobrebus_PDU test_data;


//Configuració protocol
struct BusConfig
{
  int id;
  float bps;
  unsigned long tb;
  Rol rol;
};

BusConfig bus;
//configuració del hardware del dispositiu
struct DeviceConfig
{
  int tx;
  int rx;
};

DeviceConfig device;
Chrono tb;
Chrono loop_time;

//var aux per al control:
int current_byte;
int current_bit;
SendingStatus estado;
//funcions
int sendByte(LLobrebus_PDU &pdu, int &n_byte2sent);
void sendBit (byte &data, int &n_bit2send);
void calcularCRC(LLobrebus_PDU &trama_calcular);
bool verificarTrama(LLobrebus_PDU &trama_a_verificar);

//estats
bool enviant;
/*
 * Aquí va el codi d'inicialització
 */
void setup() {
  //configurem bus 
  bus.id = 1;
  bus.bps = 88.0;
  bus.tb = 1.0/bus.bps*1000;
  bus.rol = master;
  device.tx = 2;
  device.rx = 3;
  pinMode(device.tx,OUTPUT);
  pinMode(device.rx,INPUT);

  //define test PDU
  test_data.adress = 2;
  test_data.operacio = llegir_entrada;
  test_data.data[0] = 1;
  test_data.data[1] = 0;
  test_data.data[2] = 0;
  test_data.data[3] = 0;
  test_data.crc[0] = 64; 
  test_data.crc[1] = 64;

  current_byte = 0;
  current_bit = 0;
  estado = disponible;

  enviant = false;
  #if defined(DEBUGG)
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  while(!Serial)
  {;}
  Serial.println("setup complete");
  Serial.println(bus.bps);
  Serial.println(bus.tb);
  #endif
  tb = Chrono(millis(), bus.tb);
  loop_time = Chrono(millis());
}

/*
 * Aquí va la logica del programa
 */
void loop() {
  //control de t loop (hem de procurar que no arribe mai a tb o no podrem enviar amb la freqüencia adeqüada els bits o tampoc podrem llegir-los.
  if(loop_time.getTime() > bus.tb) {
  #if defined(DEBUGG)
  Serial.print("loop_exeded:");
  Serial.print(loop_time.getTime());
  #endif
  ; //here goes error code management.
  }
  loop_time.reset();

  //Aquí bé el codi. Ara per ara enviar tota la trama
  switch(estado){
    case disponible:
      Serial.println(tb.getTime());
      if (tb.getTime() > 2000){
        estado = estado +1; //passamos al siguinte estado
        tb.reset();
      }
      break;
    case startSignal:
      if (tb.getTime() < 3.5*bus.tb){
        digitalWrite(device.tx, HIGH);
      }
      else
      {
        estado = estado +1; //passamos al siguinte estado
        tb.reset();
      }
      break;
    case sendingPDU:
      if (tb.isTime() && current_byte < LONG_MAX_TRAMA)
      {
        tb.reset();
        if (sendByte(test_data,current_byte)){
          current_byte++;
        }
      }
      else if (current_byte == LONG_MAX_TRAMA)
      {
        current_byte = 0;
        tb.reset();
        estado = stopSignal; //se ha enviado toda la PDU, passamos al siguiente estado
      }
      break;
    case stopSignal:
      if (tb.getTime() < 3.5*bus.tb){
        digitalWrite(device.tx,  HIGH);
      }
      else
      {
        digitalWrite(device.tx,  LOW);
        estado = disponible; //passamos al siguinte estado
        tb.reset();
      }
      break;
      
      
  }

  /*if(current_byte > 3 && enviant){
    ; //enviar caracters de parada i despres 1 durant 3,5 tb i acabar dixant la linia a 0
  }
  */
}

int sendByte(LLobrebus_PDU &pdu, int &n_byte2sent){
  byte data;
  switch (n_byte2sent) {
    case 0: //byte_start
      data = byte(STARTCHAR);
      break;
    case 1: //id
      data = byte(pdu.adress);
      break;
    case 2: //OPCode
      data = byte(pdu.operacio);
      break;
    case 3: //data 1
      data = byte(pdu.data[0]);
      break;
    case 4: //data 2
      data = byte(pdu.data[1]);
      break;
    case 5: //data 3
      data = byte(pdu.data[2]);
      break;
    case 6: //data 4
      data = byte(pdu.data[3]);
      break;
    case 7: //crc 1
      data = byte(pdu.crc[0]);
      break;
    case 8: //crc 2
      data = byte(pdu.crc[1]);
      break; 
    case 9: //byte_stop[1]
      data = byte(STOPCHAR[1]);
      break;
    case 10: //byte_stop[2]
      data = byte(STOPCHAR[2]);
      break;         
  }

  
  sendBit(data, current_bit);
  if (current_bit < 8){
    return 0;  
  }
  else {
    #if defined(DEBUGG)
          Serial.print("byte: ");
          Serial.println(data,DEC);
      #endif
    current_bit = 0;
    return 1;
  }
}

void sendBit (byte &data, int &n_bit2send){
  if (bitRead(data, n_bit2send)) {
      digitalWrite(device.tx, HIGH);
      digitalWrite(LED_BUILTIN, HIGH); //para mostrar que se esta enviando
//      #if defined(DEBUGG)
//          Serial.println(1);
//      #endif
    } else {
      digitalWrite(device.tx, LOW);
      digitalWrite(LED_BUILTIN, LOW); //para mostrar que se esta envaindo
//      #if defined(DEBUGG)
//          Serial.println(0);
//      #endif
    }
    n_bit2send++;
}

void calcularCRC(LLobrebus_PDU &trama_calcular){
  ;
}

bool verificarTrama(LLobrebus_PDU &trama_a_verificar){
  LLobrebus_PDU copia_trama = trama_a_verificar; 
  calcularCRC(copia_trama); //calcular en una copia el crc del missatge
  return copia_trama.crc == trama_a_verificar.crc;  //retornar la comparació (true significa que el crc rebut coincideix amb el calculat, false, la trma te algun error)
}
