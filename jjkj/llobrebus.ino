









void setup() {
  // configure pins
  pinMode(txp,OUTPUT);
  pinMode(txn,OUTPUT);
  id_dispositiu = 1;
  rol_dispositiu = master;
  // set some variables:
  start_signal_time = 3.5*Tb();
  end_signal_time = 3,5*Tb();
}

void loop() {
  //seleccionar el comportament del dispositiu sengons sigui master o esclau
  switch (rol_dispositiu){
    case master:
      doAsMaster();
      break;
    case slave:
      doAsSlave();
      break;
    default:
      //do nothing?
      breack;
  }  
}


void doAsMaster(){
  //aqui anira la logica del vostre master i el que fa i desfa, inclos quan decideix enviar missatges
}

void doAsSlave(){
  //aquí la logica de funcionament del vostre esclua (actualitzar registres i entrades o verificacio de l'estat del sensor) mentre espera una ordre
}
