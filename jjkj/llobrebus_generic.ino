/*
   LLIsta de funcions generiques utils tants per a master com per a esclau
*/

int enviar_bit(byte &data2send, int &bit2send ) {
    if (bitRead(data2send, bit2send)) {
      digitalWrite(txp, HIGH);
    } else {
      digitalWrite(txp, LOW);
    }
    bit2send++;
  return 0;
}

int leer_bit(byte &data2rcv, int &bit2rcv){
  /*if (digitalRead(rx) == HIGH)
      bitWrite(data2rcv, bit2rcv, 1);
      */
  bit2rcv++;
  return 0;
}

int enviar_byte(byte &data2send){
  int bit2send = 0;
  while (bit2send < 8) {
    //if is time to send new bit add time management
        enviar_bit(data2send,bit2send);
        

  }
  return 0; //byte enviado
}

int recivir_byte(byte &data2rcv){
  int bit2rcv = 0;
  while (bit2rcv<8) {
    //if is time to read a new bit add time managmenet
      leer_bit(data2rcv,bit2rcv);
      //return 1; //Se ha recibido un nuevo bit
  }
  return 0; //nuevo byte recibido
}

int enviar_trama(LLobrebus_PDU &trama):

int recivir_trama(Llobrebus_PDU &trama_recibida);

void calcularCRC(LLobrebus_PDU &trama_calcular);

bool verificarTrama(LLobrebus_PDU &trama_a_verificar}{
  LLobrebus_PDU copia_trama = trama_a_verificar; 
  calcularCRC(copia_trama); //calcular en una copia el crc del missatge
  return copia_trama.crc == trama_a_verificar.crc;  //retornar la comparació (true significa que el crc rebut coincideix amb el calculat, false, la trma te algun error)
}
